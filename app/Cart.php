<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    // Proctected fillable fields
    protected $fillable = [
        'products_id', 'users_id'
    ];

    // menyembunyikan model saat di panggil
    protected $hidden = [];

    // relasi ke produk
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'products_id');
    }

    // relasi ke user
    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
