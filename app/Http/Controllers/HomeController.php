<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Product;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Mengambil data Category, yang di ambil hanya 6 data
        $categories = Category::take(6)->get();
        // Mengambil data product dan merelasikan ke tabel product_galleries
        $products = Product::with(['galleries'])->latest()->take(8)->get();

        return view('pages.home', [
            'categories' => $categories,
            'products' => $products
        ]);
    }
}
