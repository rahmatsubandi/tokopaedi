<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    protected $fillable = [
        'photos', 'products_id'
    ];

    protected $hidden = [
        // 
    ];

    // Memberikan relasi ke tabel product
    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id', 'id');
    }
}
